use crate::utils::discord_utils::split_in_chunks;
use crate::utils::random_algs::lcg::LinearCongruentialGenerator;
use crate::utils::randomness_test::monte_carlo_pi_test;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::error::Error;

#[command]
pub fn lcgall(ctx: &mut Context, msg: &Message, args: Args) -> CommandResult {
    _lcg(ctx, msg, args, true);
    Ok(())
}

#[command]
pub fn lcg(ctx: &mut Context, msg: &Message, args: Args) -> CommandResult {
    _lcg(ctx, msg, args, false);
    Ok(())
}

fn _lcg(ctx: &mut Context, msg: &Message, mut args: Args, send_all: bool) {
    let mut do_steps = || -> Result<Vec<u128>, Box<dyn Error>> {
        let a: u128 = args.single()?;
        let c: u128 = args.single()?;
        let m: u128 = args.single()?;
        let seed: u128 = args.single()?;
        let amount_of_numbers: usize = args.single()?;
        let lcgenerator = LinearCongruentialGenerator::new(a, c, m, seed);
        Ok(lcgenerator.take(amount_of_numbers).collect())
    };

    let result = do_steps();
    match result {
        Ok(res) => {
            if send_all {
                let chunks = split_in_chunks(format!("{:?}", res).replace(",", ""));
                for chunk in chunks {
                    let _ = msg.channel_id.say(&ctx.http, chunk);
                }
            }
            if res.len() > 1 {
                let _ = msg.channel_id.say(
                    &ctx.http,
                    format!("pi approx : {}", monte_carlo_pi_test(res).unwrap_or(0.0)),
                );
            }
        }
        _ => {
            let _ = msg.channel_id.say(
                &ctx.http,
                format!("takes in args <a c m seed> as u128 and another arg <amount> as usize"),
            );
            let _ = msg
                .channel_id
                .say(&ctx.http, "https://www4.stat.ncsu.edu/~laber/lcg.html");
        }
    }
}
