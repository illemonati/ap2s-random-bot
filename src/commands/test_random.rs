use crate::utils::randomness_test::monte_carlo_pi_test;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::error::Error;

///Tests randomness
#[command]
pub fn randtest(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult {
    if args.len() < 2 {
        let _ = msg
            .channel_id
            .say(&ctx.http, "Must have at least 2 arguments");
        return Ok(());
    }

    let mut do_steps = || -> Result<f64, Box<dyn Error>> {
        let mut numbers = Vec::new();
        while !args.is_empty() {
            numbers.push(args.single::<u128>()?);
        }
        let pi_approx: f64 = monte_carlo_pi_test(numbers).unwrap_or(0.0);
        Ok(pi_approx)
    };

    match do_steps() {
        Ok(pi_approx) => {
            let _ = msg
                .channel_id
                .say(&ctx.http, format!("pi approx : {}", pi_approx));
        }
        _ => {
            let _ = msg.channel_id.say(
                &ctx.http,
                format!("Take a seris of positive numbers with space as seperator"),
            );
            let _ = msg.channel_id.say(
                &ctx.http,
                "example args : \" 1 12 333 123123 323 122 312323 2 \"",
            );
        }
    }

    Ok(())
}
