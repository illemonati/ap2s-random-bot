use serenity::framework::standard::{macros::command, Args, CommandError, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
pub fn echo(ctx: &mut Context, msg: &Message, args: Args) -> CommandResult {
    let thing_to_echo = args.message();
    match msg.channel_id.say(&ctx.http, thing_to_echo) {
        Ok(_) => Ok(()),
        Err(e) => Err(CommandError::from(e)),
    }
}
