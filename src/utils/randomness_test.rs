pub fn monte_carlo_pi_test(numbers: Vec<u128>) -> Option<f64> {
    if numbers.len() < 1 {
        return None;
    }

    let normalized_numbers = normalize_between_0and1(numbers);
    let n = normalized_numbers.len() - 1;
    let mut m = 0f64;
    for i in 0..normalized_numbers.len() - 2 {
        let a = normalized_numbers[i];
        let b = normalized_numbers[i + 1];
        if (a * a) + (b * b) < 1.0 {
            m += 1.0;
        }
    }
    return Some((4f64 * m) / n as f64);
}

pub fn normalize_between_0and1(numbers: Vec<u128>) -> Vec<f64> {
    let max = *numbers
        .iter()
        .fold(&0u128, |a, b| if a > b { a } else { b });
    let min = *numbers
        .iter()
        .fold(&0u128, |a, b| if a < b { a } else { b });
    let maxmmin = (max - min) as f64;
    let res: Vec<f64> = numbers
        .iter()
        .map(|&n| (n - min) as f64 / maxmmin)
        .collect();
    return res;
}
