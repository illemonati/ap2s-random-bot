use std::str;

pub fn split_in_chunks(msg: impl Into<String>) -> Vec<String> {
    let msg = msg.into();
    msg.as_bytes()
        .chunks(2000)
        .map(str::from_utf8)
        .map(|s| String::from(s.unwrap()))
        .collect::<Vec<String>>()
}
