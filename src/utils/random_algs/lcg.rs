pub struct LinearCongruentialGenerator {
    xn: u128,
    a: u128,
    c: u128,
    m: u128,
}

impl LinearCongruentialGenerator {
    pub fn new(a: u128, c: u128, m: u128, x0: u128) -> Self {
        LinearCongruentialGenerator { xn: x0, a, c, m }
    }
}

impl Iterator for LinearCongruentialGenerator {
    type Item = u128;
    fn next(&mut self) -> Option<Self::Item> {
        self.xn = (self.a * self.xn + self.c) % self.m;
        Some(self.xn)
    }
}
