mod commands;
mod utils;
use serenity::{
    framework::{standard::macros::group, StandardFramework},
    model::gateway::Ready,
    prelude::*,
};

use std::env;

use commands::*;
use log::{info, warn};

#[group]
#[commands(echo, lcg, lcgall, randtest)]
struct General;

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, _ctx: Context, ready: Ready) {
        info!("Connected ! - {}", ready.user.name);
    }
}

fn main() {
    let token = env::var("RANDOM_BOT_TOKEN").expect("no token found");
    let mut client = Client::new(&token, Handler).expect("client creation error");
    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.prefix("!!"))
            .group(&GENERAL_GROUP),
    );

    if let Err(e) = client.start() {
        warn!("Error: {:?}", e);
    }
}
